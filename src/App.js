import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Grid/styles1.css';
import './Grid/styles2.css';
import { Card, CardActions, CardHeader, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import { Responsive, WidthProvider } from 'react-grid-layout';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import _ from 'lodash';

const ResponsiveReactGridLayout = WidthProvider(Responsive);



class App extends Component {

  state = {
    cards: [1, 2],
    layouts: {
      lg: [
        { i: '0', x: 0, y: 0, w: 1, h: 1, minW: 1, maxH: 3, isDraggable: false },
        { i: '1', x: 1, y: 0, w: 1, h: 2, minW: 1, maxH: 3 },
        { i: '2', x: 0, y: 1, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '3', x: 1, y: 1, w: 1, h: 1, minW: 1, maxH: 3 }
      ],
      md: [
        { i: '0', x: 0, y: 0, w: 1, h: 1, minW: 1, maxH: 3, isDraggable: true },
        { i: '1', x: 6, y: 0, w: 5, h: 1, minW: 1, maxH: 3 },
        { i: '2', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '3', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 }
      ],
      sm: [
        { i: '0', x: 0, y: 0, w: 1, h: 1, minW: 1, maxH: 3, isDraggable: true },
        { i: '1', x: 4, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '2', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '3', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 }
      ],
      xs: [
        { i: '0', x: 0, y: 0, w: 2, h: 1, minW: 1, maxH: 3, isDraggable: true },
        { i: '1', x: 3, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '2', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '3', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 }
      ],
      xxs: [
        { i: '0', x: 0, y: 0, w: 2, h: 1, minW: 1, maxH: 3, isDraggable: true },
        { i: '1', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '2', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 },
        { i: '3', x: 1, y: 0, w: 1, h: 1, minW: 1, maxH: 3 }
      ]
    },
    breakpoint: 'lg'
  }

  render() {
    return (
      <ResponsiveReactGridLayout
        className="layout"
        layouts={this.state.layouts}
        rowHeight={165}
        cols={{ lg: 2, md: 2, sm: 2, xs: 2, xxs: 1 }}
        onBreakpointChange={(newBreakpoint, newCols) => {
          this.setState({
            breakpoint: newBreakpoint
          })
        }} onLayoutChange={(currentLayout, allLayouts) => {
          this.setState({
            layouts: allLayouts
          })
        }}>
        {
          this.state.cards.map((card, i) => {

            const height = this.state.layouts[this.state.breakpoint] ? this.state.layouts[this.state.breakpoint][i].h : null
            const classHeader = 'card-header hc' + height
            const classBody = 'card-body bc' + height
            const classFooter = 'card-footer fc' + height

            return (
              <div key={String(i)}>
                <Card className="wivo-card" style={{ borderRadius: 5 }}>
                  <button className="button-card button-card-remove">x</button>
                  <button className="button-card button-card-expand">/</button>
                  <button className="button-card button-card-minus">-</button>

                  <CardHeader
                    title="Categorías"
                    titleStyle={{ fontWeight: 400, color: '#323C47', fontSize: 20 }}
                    subtitle="Comparado con: Mes anterior"
                    subtitleStyle={{ fontWeight: 200, color: '#9B9B9B', fontSize: 15 }}
                    className={classHeader}
                  />
                </Card>
              </div>
            )
          })
        }
      </ResponsiveReactGridLayout>
    );
  }
}

export default App;
